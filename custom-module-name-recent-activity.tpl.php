<?php
/**
* @file
* Template for ajax map
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru" dir="ltr" >
	<head>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="generator" content="Joomla! - Open Source Content Management" />
  

  <script src="http://api-maps.yandex.ru/2.0.10/?load=package.full&lang=ru-RU" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>

	</head>
	<body class="contentpane">
	<!--
<div class="some class"> <?php print $variable1; ?> </div>

-->

<script type="text/javascript">
<!--
            var map, placemark;
			var Drupal;
			
            window.onload = function init () {
			ymaps.geocode(37.609218,55.753559, {results: 100}).then(function (res) {
				map = new ymaps.Map("YMapsID", {
					// Центр карты
					center: res.geoObjects.get(0).geometry.getCoordinates(),
					// Коэффициент масштабирования
					zoom: 14
		
				}
				);	
				map.controls.add("zoomControl");
				
                var point = res.geoObjects.get(0).geometry.getCoordinates();
				
				startPlacemark = new ymaps.Placemark(point, {
						draggable: true, hideIcon: false
             
						}, {
							// Опции
							// Иконка метки будет растягиваться под ее контент
							preset: 'twirl#blueStretchyIcon'
						});
						
				map.geoObjects.add(startPlacemark);
				

            });
            

            

			}
			

			
			function showAddress (value) {
				ymaps.geocode(value, {results: 100}).then(
	
                function (res) {
					if (res.geoObjects.getLength()) {
						// point - первый элемент коллекции найденных объектов
						var point = res.geoObjects.get(0);
						// Добавление полученного элемента на карту
						map.geoObjects.add(point);

						document.getElementById('coords').value = point.geometry.getCoordinates()[0];
						document.getElementById('coords2').value = point.geometry.getCoordinates()[1];
						
						if(window.parent.document.getElementById('edit-yandexmap-default-longitude') != null) {
							window.parent.document.getElementById('edit-yandexmap-default-longitude').value = point.geometry.getCoordinates()[1];
							window.parent.document.getElementById('edit-yandexmap-default-latitude').value = point.geometry.getCoordinates()[0];
						} else if(window.parent.document.getElementById('edit-yandexmap-marker-latitude') != null) {
							window.parent.document.getElementById('edit-yandexmap-marker-longitude').value = point.geometry.getCoordinates()[1];
							window.parent.document.getElementById('edit-yandexmap-marker-latitude').value = point.geometry.getCoordinates()[0];
						}	
						// Центрирование карты на добавленном объекте
						map.panTo(point.geometry.getCoordinates());
					}
				},
				// Обработка ошибки
				function (error) {
					alert("Возникла ошибка: " + error.message);
				}
                    
                );
            }
            --></script>

<div>
  <form action="#" onsubmit="showAddress(this.address.value);return false;">
    <table>
      <tr>
	   <td><?php echo t('Click on map to get your coords');?>:</td>
        <td><input name=""  type="text" id="address" value="" size="30" />
         </td>
        <td><input class="find-button" type="submit" value="<?php echo t('Save Coords');?>">
         
        </td>
      </tr>
      <tr>
        <td colspan="3">
        	<?php echo t('Latitude');?>:&nbsp;<input name="" type="text" id="coords" size="30"></input>
        </td>
      </tr>
	   <tr>
        <td colspan="3">
            <?php echo t('Longitude');?>:&nbsp;<input name="" type="text" id="coords2" size="30" /></input>
        </td>
      </tr>
	  
    </table>

    <div style="margin:0;padding:0;text-align:center;">
      <div id="YMapsID" style="margin:0;padding:0;width:650px;height:300px;margin-bottom:0px;"></div>
    </div>
  </form>
  </td>
  <td><i></i></td>
  </tr>
  </table>
 </div>
</body>
</html>