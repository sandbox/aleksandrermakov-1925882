<?php 
/**
 * @file
 * 
 */

function yandexmap_admin_settings($form, &$form_state) {

  $defaults = yandexmap_defaults();
  return yandexmap_form($form, $form_state);

}
/**
 * Handle correct storage of several MarkerClusterer settings.
 */
function yandexmap_admin_settings_markerclusterer_submit($form, &$form_state) {
  
}



function yandexmap_create_marker($form, &$form_state, $args = '') {
  
    
    $form = array();
    $form['#attributes']['id'] = 'yandexMapMarker';
    $form['#attributes']['name'] = 'yandexMapMarker';

  

    if (empty($args)) {
      
      $yid = 1;
  
    }
     else {
  
      $yid = $args;
    }
    $query = db_select('ymap')
    ->extend('PagerDefault')
    ->fields('ymap');
    
    $result = $query->execute();

    
    if ($result->rowCount() > 0) {
      $query = db_select('map_yandex_metki')
      ->extend('PagerDefault')
      ->fields('map_yandex_metki')
      ->condition('id', $yid)
      ->orderBy('checked_out_time', 'DESC')
      ->limit(20);
    
      $result = $query->execute();
    
      if ($result->rowCount() > 0) {
        $valdb = array();
        foreach ($result as $row) {
          $valdb[] = $row;
      
        }
        $valdb = $valdb[0];
      } 
      else {
        $valdb = yandexmap_default_marker();
      }
      $form['#attributes']['no-map'] = array('#value' => 1);
    } 
    else {
      drupal_set_message(t('Before make marker you need create at least one map!'));
      $form['#attributes']['no-map'] = array('#value' => 0);
      return $form;
      
      
    }

  
    $defaults = yandexmap_defaults();
    $bp = base_path();
    drupal_add_library('system', 'ui.dialog');
    drupal_add_js('
  
      $j = jQuery;
      $j(function(){
  

      $j(".dialogforoords").click(function(e){
    
      e.preventDefault();
      if ($j("#mymaps").length == 0) {
      $j("#dialog").append($j("<iframe id=\"mymaps\" width=\"700\" height=\"590\" frameborder=\"0\" />").attr("src",
      "' . $bp . 'admin/config/services/yandexmap/getajaxmap")).dialog({
      width: 800,
      height: 600
});
} else {
      $j("#dialog").dialog("open");
}
});
  
});', 'inline');
  
  
    $query = db_select('ymap')
    ->extend('PagerDefault')
    ->fields('ymap')
    ->orderBy('yid', 'DESC');
    
    $result = $query->execute();
    
    $maps = array();
    foreach ($result as $row) {
      $maps[$row->yid] = $row->name_map_yandex;
    
    }
    
  
    $form['yandexmap_marker'] = array(
        '#type' => 'fieldset',
        '#title' => t('Default marker settings'),
        // This will store all the defaults in one variable.
        '#tree' => TRUE,
        '#weight' => '1'
    );
    $form['yandexmap_marker']['title'] = array(
        '#type' => 'textfield',
        '#description' => t('Name of marker'),
        '#size' => '25',
        '#default_value' => $valdb->name_marker,
    );
    
    $form['yandexmap_marker']['id_map'] = array(
        '#type' => 'select',
        '#title' => t('Select map for marker'),
        '#attributes' => array('autocomplete' => 'off'),
        '#options' => $maps,
        '#default_value' => $valdb->id_map,
    
    );
    
    $form['yandexmap_marker']['conditions'] = array(
        '#type' => 'select',
        '#title' => t('Conditions'),
        '#options' => array(
            '1' => t('Adress'),
            '2' => t('Coords'),
    
        ),
        '#default_value' => $valdb->yandexcoord,
    
    );
    $form['yandexmap_marker']['city_map_yandex'] = array(
        '#type' => 'textfield',
        '#title' => t('City?'),
        '#default_value' => $valdb->city_map_yandex,
        '#states' => array(
            'visible' => array(
                ':input[name="yandexmap_marker[conditions]"]' =>  array('value' => '1'),
            ),
        ),
    );
    
    $form['yandexmap_marker']['street_map_yandex'] = array(
        '#type' => 'textfield',
        '#title' => t('Adress?'),
        '#default_value' => $valdb->street_map_yandex,
        '#states' => array(
            'visible' => array(
                ':input[name="yandexmap_marker[conditions]"]' =>  array('value' => '1'),
            ),
        ),
    );

  $lng =  drupal_json_decode($valdb->lng);

  if (empty($lng)) {
    $lng['latitude'] = 0;
    $lng['longitude'] = 0;
  }
  $form['yandexmap_marker']['latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('latitude?'),
      '#default_value' => $lng['latitude'],
      '#states' => array(
          'visible' => array(
              ':input[name="yandexmap_marker[conditions]"]' =>  array('value' => '2'),
          ),
      ),
  );



  $form['yandexmap_marker']['longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('longitude?'),
      '#default_value' => $lng['longitude'],
      '#states' => array(
          'visible' => array(
              ':input[name="yandexmap_marker[conditions]"]' =>  array('value' => '2'),
          ),
      ),
  );


  $form['yandexmap_marker']['yandexbutton'] = array(
      '#type' => 'button',

      '#default_value' => t('Open map'),
      '#states' => array(
          'visible' => array(
              ':input[name="yandexmap_marker[conditions]"]' =>  array('value' => '2'),
          ),
      ),

  );
    
    $form['yandexmap_marker']['misil'] = array(
        '#type' => 'text_format',
        '#title' => t('Description of main balloon'),
        '#default_value' => $valdb->misil,
        '#weight' => 3,
    );
    $form['yandexmap_marker']['misilonclick'] = array(
        '#type' => 'text_format',
        '#title' => t('Description of main balloon after click'),
        '#default_value' => $valdb->misilonclick,
        '#weight' => 4,
    );
    
    
  
    $form['yandexmap_marker']['html'] = array(
        '#value' => t('<a href="http://ykyuen.wordpress.com">Eureka!</a>'),
        '#type' => 'html_markup',
        
        );
    

    if (!empty($args)) {
      $form['yandexmap_marker']['idmarker'] = array(
          '#type' => 'hidden',
          '#default_value' => $args,
          '#value' => $args,
            
      );
    }
    
    $form['yandexmap_marker']['dialogdiv'] = array(
        '#markup' => '<div id="dialog" title="' . t('Select You Adress!') . '">
    
      </div>'
    );
    
    
    $form['yandexmap_marker']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save marker...'),
        '#name' => 'add_more',
        // Элемент теперь называется ajax

        '#id' => 'myuniqueid',
        '#weight' => '21'
    );
    return $form;
  
  
  
}


function yandexmap_create_marker_submit($form, &$form_state) {
  

  
  $user = $uid = $GLOBALS['user']->uid;
  $name = $form['yandexmap_marker'];

  if (empty($_POST['deficon'])) {
    $_POST['deficon'] = 'blackStretchyIcon';
  }
  $lng = 1;
  if ($name['conditions']['#value'] == 2) {
    $lng = json_encode(array('longitude' => $name['longitude']['#value'], 'latitude' => $name['latitude']['#value']));
  }
  
  $gmd = gmstrftime("%Y-%m-%d");
  if (!empty($name['idmarker']['#value'])) {
    
    $nid = db_update('map_yandex_metki')
    ->fields(array(
        'name_marker' => $name['title']['#value'],
        'misil' => $name['misil']['value']['#value'],
        'misilonclick' => $name['misilonclick']['value']['#value'],
        'city_map_yandex' => $name['city_map_yandex']['#value'],
        'street_map_yandex' => $name['street_map_yandex']['#value'],
        'checked_out_time' => $gmd,
        'published' => 1,
        'yandexcoord' => $name['conditions']['#value'],
        'lng' => $lng,
        'id_map' => $name['id_map']['#value'],
        'deficon' => $_POST['deficon'],
        'who_add' => $user,
        'params' => 0,
    
    ))
    ->condition('id', $name['idmarker']['#value'])
    ->execute();
    if ($nid) {
      drupal_set_message(t('The marker has been updated.'));
    }
  } 
  else {
    
  
  
  $nid = db_insert('map_yandex_metki') 
  ->fields(array(
      'name_marker' => $name['title']['#value'],
      'misil' => $name['misil']['value']['#value'],
      'misilonclick' => $name['misilonclick']['value']['#value'],
      'city_map_yandex' => $name['city_map_yandex']['#value'],
      'street_map_yandex' => $name['street_map_yandex']['#value'],
      'checked_out_time' => $gmd,
      'published' => 1,
      'yandexcoord' => $name['conditions']['#value'],
      'lng' => $lng,
      'id_map' => 1,
      'deficon' => $_POST['deficon'],
      'who_add' => $user,
      'params' => 0,
  
  ))
  ->execute();
  drupal_set_message(t('The marker has been saved.'));
  }
  
  
}


function yandexmap_marker_items($args = '') {
  if ($args) {

    $output = drupal_render(drupal_get_form('yandexmap_create_marker', $args));

    return $output;
  }
  
  // Create a drop down menu
  $links = array();
  $links[] = array(
      'title' => t('Link 1'),
      'href' => 'av',
  );
  
  
  
  
  $output = '';
  

  $query = db_select('map_yandex_metki')
  ->extend('PagerDefault')
  ->fields('map_yandex_metki')
  ->orderBy('id', 'DESC')
  ->limit(5);
  
  $result = $query->execute();
  
  if ($result->rowCount() == 0) {
    drupal_set_message(t('No marker! Create it!'));
    $output .= l(t('Create marker!'), 'admin/config/services/yandexmap/createmarker');
    return $output;
  }
  foreach ($result as $row) {
    //$output .= $row->name.'<br/>';
    $link = l($row->name_marker, 'admin/config/services/yandexmap/markeritems/' . $row->id);
    $menu = array(
        'links' => array(
            'edit' => array(
                'title' => t('Edit'),
                'href' => 'admin/config/services/yandexmap/markeritems/' . $row->id,
            ),
            'delete' => array(
                'title' => t('Delete'),
                'href' => 'admin/config/services/yandexmap/deletemarker/' . $row->id,
            ),
        ),
    );
    $ctb = theme('links__ctools_dropbutton', $menu);
  
    $rows[] = array($row->name_marker, $link, '<a href="">' . $ctb . '</a>');
  
  }
  /*
   foreach($names as $mn =>$title) {
  $rows[] = array($title,'<a href="">'.$output.'</a>','');
  }
  */
  
  $header = array(t('Title'), t('Description'), t('Function'));
  
  $group = theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('width' => '100%')
  ));
  
  $output = $group . theme('pager');
  
  return $output;
}

function yandexmap_delete_marker($args = '') {
  
  $query = db_delete('map_yandex_metki')->condition('id', $args);
  
  
  $result = $query->execute();
  if ($result) {
    drupal_goto('admin/config/services/yandexmap/markeritems');
  }
}
function yandexmap_delete_map($args = '') {

  $query = db_delete('ymap')
  ->condition('yid', $args);


  $result = $query->execute();
  if ($result) {
    drupal_goto('admin/config/services/yandexmap/allmaps');
  }
}
function yandexmap_default_marker($args = '') {
  $valdb = new stdClass();
  
  $valdb->yandexcoord = '';
  $valdb->name_marker = '';
  $valdb->city_map_yandex = '';
  $valdb->street_map_yandex = '';
  $valdb->id_map = '';
  $valdb->name_map_yandex = '';
  $valdb->misil = '';
  $valdb->misilonclick = '';
  $valdb->lng = '';
  return $valdb;  
}

