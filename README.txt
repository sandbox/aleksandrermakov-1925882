-- SUMMARY --

Module Yandex Map for Drupal 7-x
Module Yandex Map - allows insert Yandex Maps everywhere YOU want!
Module Yandex Map use  Yandex map API version 2.0.10

-- FEATURES --
* Insert map in text post as tag {modmapyandex_id=you id map}.
* Insert Yandex map as common article page.
* You can insert more than one map in post
* Add marker on map, select type of marker, color, address.
* Change settings in administrator interface for delete, modify, add maps and markers.
* Insert map without keys

-- REQUIREMENTS --

The module uses the Fields API.
Requires the field module.
For beautiful button required Ctools (optional). If CTools not install button style was as common link.

-- INSTALLATION --

Install as usual.
See http://drupal.org/documentation/install/modules-themes/modules-7
for further information.
