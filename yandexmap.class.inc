<?php
/**
 * @file
 * Main class for output Yandex map in post or as article 
 */

class MapYandexClass {



  public function getMap($defaults, $id, $idcount, $type) {


    $this->_id = $id;
    $foobar = $this->getFoobar($id, $type);
    if (!$foobar) return '';
    $this->metka = $this->getMetka($foobar->yid);
    
    
    $content='';
    $metka = '';
    $metka .= 'myGeoObjects = [];';

    $draggable_placemark = 'draggable: true, // Метку можно перетаскивать, зажав левую кнопку мыши . ';
    $i = 0;
    foreach ($this->metka as $val) {
    ++$i;
      
      $op = '
      { balloonCloseButton: true,
          draggable: true,
          preset: \'twirl#' . $val->deficon . '\'
    }';
        
      //size of marker
      if (!empty($val->wih)) {
        $wih = json_decode($val->wih);
        if (count($wih) == 0 || !$wih) {
          $wih[0] = 250;
          $wih[1] = 250;
        }
      } 
      else {
        $wih[0] = 250;
        $wih[1] = 250;
      }
      if (preg_match('@Stretchy@s', $val->deficon, $m)) {
        $forstrchmarkertext = addslashes($val->misilonclick);
      }
       else {
        $forstrchmarkertext = '';
      }

      if ($val->yandexcoord == 1) {

      

        $metka .= '
            /* После того, как поиск вернул результат, вызывается */
            ymaps.geocode(\'' . $val->city_map_yandex . ', ' . $val->street_map_yandex . '\', {results: 100}).then(function (res) {

                var point = res.geoObjects.get(0).geometry.getCoordinates();

                var properties = {
                balloonContent: "' . addslashes($val->misil) . '",
                    iconContent: "' . $forstrchmarkertext . '",
                        hintContent: "<div>' . addslashes($val->misilonclick) . '</div>"

      }


                            myGeoObject = new ymaps.GeoObject({
                      
                            geometry: {
                            type: "Point",
                            coordinates: point
      },
                      
                            properties: properties
      },
                            ' . $op . '
                                );
                                /* Добавляем геообъект на карту */
                                map' . $idcount . ' . geoObjects.add(myGeoObject);


      });  ';

      } 
      else {
      
        $lng = json_decode($val->lng);

        $metka .= '
            var properties = {
            balloonContent: "' . addslashes($val->misil) . '",
                iconContent: "' . addslashes($val->misilonclick) . '",
                    hintContent: "' . addslashes($val->misilonclick) . '"


      },
                            myGeoObject = new ymaps.GeoObject({
                            geometry: {
                            type: "Point",
                            coordinates: [' . $lng->latitude . ', ' . $lng->longitude . ']
      },
                                /* Описываем данные геообъекта */
                                properties: properties
      },
                                ' . $op . '
                                    );
                                    /* Добавляем геообъект на карту */
                                    map' . $idcount . ' . geoObjects.add(myGeoObject);
                                    ';
      }



    }

    
    //$getmetka = $this->getMetka($id);


    if ($foobar) {
      ($foobar->map_type == 'calculator') ? $this->map_type = 'calculator' : $this->map_type = 'map';

        
      if ($foobar->bradius == 1) {
        $borderradius = 'border-radius: 6px 6px 6px 6px;';
      }
       else {
        $borderradius = '';
      }
      
      if ($foobar->yandexborder == 1) {
        $border = 'border: 1px solid #' . $foobar->color_map_yandex . ';';
      } 
      else {
        $border = '';
      }
      if ($foobar->center_map_yandex == 1) {
        $margin = 'margin:0 auto;';
      }
       else {
        $margin = '';
      }
        
      $style = ' . YMaps-b-balloon-wrap td {
          padding:0!important;
    }
          #YMapsID-' . $idcount . ' {
              margin:0;
              
                  background: -moz-linear-gradient(center top , #' . $foobar->color_map_yandex . ', #F1F1F1) repeat scroll 0 0 #F1F1F1;
                      color: #333333;
                      font-weight: bold;
                      ' . $borderradius . '
                          ' . $border . '
                              ' . $margin . '

    }
                                  .YMaps-b-balloon-content {
                                  width:' . $foobar->oblako_width_map_yandex . 'px !important;
    }
                                      .imginmap {
                                      margin:0 5px 0 0;
    }
                                      
                                      #linkslyweb {
                                        ' . $margin . '
                                      }
                                      ';
    
      drupal_add_css($style, 'inline');

      $userpath = '';

      if (empty($userpath)) {

        $userpath = '/images/mapyandex/yandexmarkerimg/';
      }




      $textarray = '';
      $textarrayonput = '';
      $route = json_decode($foobar->route_map_yandex);
      $ymaproute = '';


      //$ymaproute = $this->addRouteToMap($route, $foobar, $id, $idcount);

      ($foobar->map_baloon_autopan) ? $autopan = 'true' : $autopan = 'false';
      ($foobar->map_centering == 1) ? $map_centering = 'true' : $map_centering = 'false';
      if (!$foobar->map_baloon_or_placemark) {

        $balloonorplacemark = 'startPlacemark = new ymaps.Placemark(point_' . $idcount . ', {
            /* Свойства */
            /* Текст метки */
            iconContent: \'' . addslashes($foobar->misil) . '\',
                hintContent: "<div>' . addslashes($foobar->misilonclick) . '</div>",
                    balloonContentHeader: "<div>' . addslashes($foobar->misilonclick) . '</div>"
                         
      }, {
                        /* Опции
                        Иконка метки будет растягиваться под ее контент */
                        preset: \'twirl#blueStretchyIcon\'
      });
                        map' . $idcount . ' . geoObjects.add(startPlacemark);';

      } 
      else {
        if (empty($foobar->map_baloon_minwidth)) {

          $foobar->map_baloon_minwidth = 100;
        }

        if (empty($foobar->map_baloon_minheight)) {

          $foobar->map_baloon_minheight = 100;
        }

        $balloonorplacemark = 'map' . $idcount . ' . balloon.open(
            /* Координаты балуна */
            point_' . $idcount . ', {
                /* Свойства балуна:
                - контент балуна */
                content: \'' . addslashes($foobar->misil) . '\'
      }, {
                    /* Опции балуна:
                 - балун имеет копку закрытия */
                    closeButton: true,
                    minWidth: ' . $foobar->map_baloon_minwidth . ',
                        minHeight: ' . $foobar->map_baloon_minheight . ',
                            autoPan: ' . $map_centering . ',
                                autoPanDuration: 1
      }
                                );';

      }

      if ($foobar->where_text == 2) {
        $content .= $foobar->text_map_yandex;
      }

      if ($foobar->yandexcoord == 1) {
        $stylecoo='style="display:none;"';
        $valone = 'var valone_' . $idcount . ' = "' . $foobar->city_map_yandex . ', ' . $foobar->street_map_yandex . '"';
      } 
      else {
        $stylead = 'style="display:none;"';
        $parsejson = json_decode($foobar->lng, TRUE);
        
        $lang = $parsejson['longitude'];
        $lat = $parsejson['latitude'];
        $valone = 'var valone_' . $idcount . ' = "' . $lat . ', ' . $lang . '"';
      }

      $autozoom = 'var autozoom = ' . $foobar->autozoom . ';';
      if ($foobar->autozoom) {
        $autozoomflag = 10;
      }
       else {
        $autozoomflag = $foobar->yandexzoom;
      }

      $el = json_decode($foobar->yandexel, TRUE);
      $lineika = '';
      $minimap = '';
      $sputnik = '';
      $search   = '';
      $scale    = '';

      if ($el) {

        if (in_array(1, $el)) {
          $lineika = ' . add("mapTools")';

        }
        if (in_array(2, $el)) {
          $minimap = ' . add("miniMap")';
        }
        if (in_array(3, $el)) {
          $sputnik = ' . add("typeSelector")';

        }
        if (in_array(4, $el)) {
          $search = ' . add("searchControl")';
        }
        if (in_array(5, $el)) {
          $scale = ' . add("zoomControl")';
        }
      } 
      else {

        $lineika = ' . add("mapTools")';
        $minimap = ' . add("miniMap")';
        $sputnik = ' . add("typeSelector")';
        $search   = ' . add("searchControl")';
        $scale    = ' . add("zoomControl")';
      }

      if ($foobar->yandexbutton == 1 || $this->map_type =='calculator') {
        $element = "
            /* Добавление элементов управления */
            " . $sputnik . "
                " . $lineika . "
                    " . $minimap . "
                        " . $scale . "
                            " . $search . "";
      }
       else {
        $element = '';
      }

      //$settings = json_decode($foobar->map_calculator_settings);
      $defaultmap = ($foobar->defaultmap) ? $foobar->defaultmap : 'publicMap';

      $textarray = '';
      $textarrayonput = '';
      $region = json_decode($foobar->region_map_yandex);

      $map_region_style = json_decode($foobar->map_region_style);
      if (!is_array($map_region_style) || empty($map_region_style)) {
        $styleoption = '
            strokeWidth: 6,
            strokeColor: \'#0000FF\', // синий
            opacity: \'0.5\', // синий
            fillColor: \'#FFFF00\', // желтый
            draggable: true      // объект можно перемещать, зажав левую кнопку мыши';
      } 
      else {
        $styleoption = '
            strokeWidth: 6,
            strokeColor: \'#' . $map_region_style[1] . '\', // синий
                opacity: \'' . $map_region_style[0] . '\', // синий
                    fillColor: \'' . $map_region_style[1] . '\', // желтый
                        draggable: true      // объект можно перемещать, зажав левую кнопку мыши';
      }
      $ymapregion = '';


      if ($this->map_type == 'map') {

        $script ='

            ymaps.ready(function () {

            var map' . $idcount . ';
                ' . $autozoom . '
                    ' . $valone . '

                        if (valone_' . $idcount . ' == "") {
                            valone_' . $idcount . ' = "Москва, ул. Ленина, 50";
      }

                                /* После того, как поиск вернул результат, вызывается*/
                                ymaps.geocode(valone_' . $idcount . ', {results: 100}).then(function (res) {

                                    var point_' . $idcount . ' = res.geoObjects.get(0).geometry.getCoordinates();
                                        /* Добавление полученного элемента на карту */
                                          
                                          
                                        map' . $idcount . ' = new ymaps.Map("YMapsID-' . $idcount . '", {
                                            /* Центр карты */
                                            center: res.geoObjects.get(0).geometry.getCoordinates(),
                                            /* Коэффициент масштабирования */
                                            zoom: ' . $autozoomflag . ',
                                                type: "yandex#' . $defaultmap . '"
      });
                                                      
                                                    if (autozoom) {

                                                    map' . $idcount . ' . setCenter(point_' . $idcount . ', ' . $foobar->yandexzoom . ', {
                                                        checkZoomRange: true,
                                                        duration: 1000,
                                                        callback:function(){
                                                        ' . $metka . '
      }
      });
      } else {


                                                            map' . $idcount . ' . zoomRange.get(
                                                                /* Координаты точки, в которой определяются
                                                                значения коэффициентов масштабирования */
                                                                point_' . $idcount . ')
                                                                    .then(function (zoomRange, err) {
                                                                      
                                                                    var userzoom = ' . $foobar->yandexzoom . ';
                                                                        if (!err) {

                                                                        /* zoomRange[0] - минимальный масштаб
                                                                           zoomRange[1] - максимальный масштаб */
                                                                        if (userzoom > zoomRange[1]) {
                                                                        userzoom = zoomRange[1];

                                                                        map' . $idcount . ' . setCenter(point_' . $idcount . ', userzoom,{duration:500,callback:function(){
                                                                            ' . $metka . '
      }});

      } else {
                                                                                ' . $metka . '
      }

                                                                                      
      }
      }
                                                                                    )
      }
                                                                                    ' . $balloonorplacemark . '


                                                                                        // Добавление стандартного набора кнопок
                                                                                        map' . $idcount . ' . controls
                                                                                            ' . $element . '
                                                                                                  
                                                                                                ' . $ymaproute . '
                                                                                                    ' . $ymapregion . '
      });

                                                                                                          
      });';
      } 
      else {
        if ($this->use_jquery) {
          if (!$this->here_jquery) {
            $jv = '1.7.2';
            $document->addScript('https://ajax.googleapis.com/ajax/libs/jquery/' . $jv . '/jquery.min.js');
            $this->here_jquery = TRUE;
          }
        }
        $script ='
            ymaps.ready(function () {

            var map' . $idcount . ';
                ' . $valone . '

                    if (!valone_' . $idcount . ') {
                        valone_' . $idcount . ' = "Санкт-Петербург, пр. Невский, 100";
      }

                            /* После того, как поиск вернул результат, вызывается*/
                            ymaps.geocode(valone_' . $idcount . ', {results: 100}).then(function (res) {

                                var point = res.geoObjects.get(0).geometry.getCoordinates();
                                // Добавление полученного элемента на карту

                                /* Создание экземпляра карты и привязка его к контейнеру div */
                                map' . $idcount . ' = new ymaps.Map("YMapsID-' . $idcount . '", {
                                    // Центр карты
                                    center: res.geoObjects.get(0).geometry.getCoordinates(),
                                    /* Коэффициент масштабирования */
                                    zoom: 12,
                                    type: "yandex#' . $defaultmap . '"

      }
                                        );
                                        $j = jQuery.noConflict();
                                        calculator = new DeliveryCalculator(map' . $idcount . ', point);





                                            // Добавление стандартного набора кнопок
                                            map' . $idcount . ' . controls
                                                ' . $element . '
                                                      
      });
                                                      
                                                      
      });

                                                    var DELIVERY_TARIF = 15,
                                                    MINIMUM_COST = 100;


                                                    function DeliveryCalculator(map, finish) {
                                                    this._map = map;
                                                    this._start = null;
                                                    this._finish = null;
                                                    this._route = null;
                                                    map.events.add(\'click\', this._onClick, this);
      }

                                                    var ptp = DeliveryCalculator.prototype;

                                                    ptp._onClick = function (e) {
                                                    var position = e.get(\'coordPosition\');
                                                    $j(\'#deletemenu\').remove();
                                                      
                                                    if (!this._start) {
                                                    this._start = new ymaps.Placemark(position, { iconContent: "А" }, { draggable : true });
                                                    this._start.events.add(\'dragend\', this._onClick, this);
                                                    this._map.geoObjects.add(this._start);
                                                    this._start.events.add(\'contextmenu\',this._onContextmenu, this);

                                                      
      } else if (!this._finish) {
                                                    this._finish = new ymaps.Placemark(position, { iconContent: "Б" }, { draggable : true });
                                                    this._finish.events.add(\'dragend\', this._onClick, this);
                                                    this._map.geoObjects.add(this._finish);
                                                    this._finish.events.add(\'contextmenu\',this._onContextmenu, this);
      } else {
                                                    /*this._map.geoObjects.remove(this._start);
                                                    this._start = null;
                                                    this._map.geoObjects.remove(this._finish);
                                                    this._finish = null;
                                                    this._map.geoObjects.remove(this._route);

                                                    this._route = null;*/
      }
                                                    this.getDirections();
      };

                                                    ptp._onContextmenu = function(e) {
                                                      
                                                    // Отключаем стандартное контекстное меню браузера
                                                    e.get(\'domEvent\').callMethod(\'preventDefault\');
                                                    // Если меню метки уже отображено, то убираем его при повторном нажатии правой кнопкой мыши
                                                    if ($j(\'#deletemenu\').css(\'display\') == \'block\') {
                                                    $j(\'#deletemenu\').remove();
      } else {
                                                    // HTML-содержимое контекстного меню.
                                                    var menuContent =
                                                    \'<div id="deletemenu">\
                                                    <ul id="menu_list">\
                                                    <li><a class="delroute" href="#">Удалить маршрут!</a></li>\
                                                    </ul>\
                                                    </div>\';
                                                    // Размещаем контекстное меню на странице
                                                    $j(\'body\').append(menuContent);

                                                    // ... и задаем его стилевое оформление.
                                                    $j(\'#deletemenu\').css({
                                                    position: \'absolute\',
                                                    left: e.get(\'position\')[0],
                                                    top: e.get(\'position\')[1],
                                                    background: \'#ffffff\',
                                                    border: \'1px solid #cccccc\',
                                                    \'border-radius\': \'12px\',
                                                    width: \'150px\',
                                                    \'z-index\': 2
      });

                                                    $j(\'#deletemenu ul\').css({
                                                    \'list-style-type\': \'none\',
                                                    padding: \'20px\',
                                                    margin: 0
      });

                                                    // Заполняем поля контекстного меню текущими значениями свойств метки.
                                                    $j(\'#deletemenu input[name="icon_text"]\').val(this._start.properties.get(\'iconContent\'));
                                                    $j(\'#deletemenu input[name="hint_text"]\').val(this._start.properties.get(\'hintContent\'));
                                                    $j(\'#deletemenu input[name="balloon_text"]\').val(this._start.properties.get(\'balloonContent\'));
                                                    var tojQuery = this;
                                                      
                                                    // При нажатии на кнопку "Сохранить" изменяем свойства метки
                                                    // значениями, введенными в форме контекстного меню.
                                                    $j(\' . delroute\').click(function (e) {
                                                    e.preventDefault();
                                                    tojQuery._map.geoObjects.remove(tojQuery._start);
                                                    tojQuery._start = null;
                                                    tojQuery._map.geoObjects.remove(tojQuery._finish);
                                                    tojQuery._finish = null;
                                                    tojQuery._map.geoObjects.remove(tojQuery._route);
                                                    tojQuery._route = null;
                                                    $j(\'#deletemenu\').remove();
      });
      }
      }

                                                    ptp.getDirections = function () {
                                                    var self = this,
                                                    start = this._start.geometry.getCoordinates();
                                                    if (!this._finish) {
                                                    return;
      }
                                                    finish = this._finish.geometry.getCoordinates();
                                                    this._route && this._map.geoObjects.remove(this._route);
                                                    ymaps.geocode(finish)
                                                    .then(function (geocode) {
                                                    var address = geocode.geoObjects.get(0) && geocode.geoObjects.get(0).properties.get("balloonContentBody") || "";

                                                    ymaps.route([start, finish])
                                                    .then(function (router) {
                                                    var delimetr = 1;
                                                    if (delimetr == 1) {
                                                    var distance = Math.round(router.getLength() / 1000);
                                                    var d = \'км\';
      }
                                                    else {
                                                    var distance = Math.round(router.getLength());
                                                    var d = \'м\';
      }
                                                    var cleardistance = Math.round(router.getLength());
                                                    textcuur = \'рублей\';

                                                      
                                                    message = \'<span>Расстояние: \' + distance + \' \' + d +\' . </span><br/>\' +
                                                    \'<span style="font-weight: bold; font-style: italic">Стоимость доставки: %s (\' + textcuur + \'). </span>\';

                                                    self._route = router.getPaths();
                                                    self._route.options.set({ strokeWidth: 5, strokeColor: \'0000ffff\', opacity: 0.5 });
                                                    self._map.geoObjects.add(self._route);
                                                    self._route.events.add(\'contextmenu\',self._onContextmenu, self);
                                                    self._finish.properties.set("balloonContentBody", address + message.replace(\'%s\', self.calculate(distance)));
                                                    self._finish.balloon.open();
      });
      });
      };

                                                    ptp.calculate = function (len) {
                                                    pr = ' . $settings[0] . ';
                                                        min = ' . $settings[1] . ';

                                                            if (!pr) pr = 0;
                                                            var cost = len * pr;

                                                            return cost < min && min || cost;
      };




                                                            ';
      }
      drupal_add_js($script, array('type' => 'inline', 'scope' => 'header', 'weight' => 2));
      $height = $foobar->height_map_yandex . 'px';
      $width = $foobar->width_map_yandex . 'px';
  
      if (strpos($foobar->height_map_yandex, 'px')) {
    
        $height = $foobar->height_map_yandex;
      }
      if (strpos($foobar->width_map_yandex, 'px')) {
        $width = $foobar->width_map_yandex;
      }
      if (strpos($foobar->height_map_yandex, '%')) {
    
        $height = $foobar->height_map_yandex;
      }
      if (strpos($foobar->width_map_yandex, '%')) {
        $width = $foobar->width_map_yandex;
      }
     
 
      $content .= ' <div id="YMapsID-' . $idcount . '" style="height:' . $height . '; width:' . $width . ';' . $margin . '"></div>

          <div id="linkslyweb" style="width:' . $width . ';text-align:right;clear:both; font-size:10px;' . $margin . '"><a href="http://slyweb.ru/yandexmap/" title="Карты для Drupal от Яндекс">Карты Яндекс для Drupal</a></div>

              <div style="height:50px;" class="clear"></div>';


      if ($foobar->where_text == 3) {
        $content .= $foobar->text_map_yandex;
      }

    } 
    else {
      $content .= '';

    }


    return $content;
  }



  /**
   * Возвращаем данные
   * @return array Возврату подлежит массив объектов
   */
  function getFoobar($id, $type) {

    $cond = 'yid';
    if ($type) {
      $cond = 'nid';
    }


    $query = db_select('ymap')
    ->extend('PagerDefault')
    ->fields('ymap')
    ->condition($cond, $id)
    ->orderBy('checked_out_time', 'DESC')
    ->limit(20);

    $result = $query->execute();
    if ($result->rowCount() == 0) {
     drupal_set_message(t('No map with id @id! Create it!', array('@id' => $id))); 
     return FALSE;
    }
     else {
        $valdb = array();
        foreach ($result as $row) {
        $valdb[] = $row;
    }

        $valdb = $valdb[0];
        return $valdb;
    }
  }

  /**
   * Возвращаем метки
   * @return array Возврату подлежит массив объектов
   */
  function getMetka($id) {
    
    $query = db_select('map_yandex_metki')
    ->extend('PagerDefault')
    ->fields('map_yandex_metki')
    ->condition('id_map', $id)
    ->orderBy('id', 'DESC')
    ->limit(20);

    $result = $query->execute();


    return $result;



  }
  function number($matches) {
    $this->idfind = $this->idfind+1;

    return '{modmapyandex_id=' . $this->idfind . '}';

  }
  function plgJSMarks( $params, $body ) {
    static $pluginParams = NULL;


    if (preg_match('@{modmapyandex_id=(.*?)|modmapyandex_calculator_id=(.*?)}@si', $body)) {

      
       drupal_add_js('http://api-maps.yandex.ru/2.0.10/?load=package.full&mode=debug&lang=ru-RU', array('type' => 'external', 'scope' => 'header', 'weight' => 1, 'cache' => TRUE));
      preg_match_all('@{(modmapyandex_id|modmapyandex_calculator_id)=(.*?)}@si', $body, $id);
        
      //замена одинаковых карт
      $this->idfind = 0;
      $body = preg_replace_callback('@{modmapyandex_id=.*?}|{modmapyandex_calculator_id=.*?}@si', array(&$this, 'number'), $body);
      //замена разметки на html
      $idcount = 0;
      foreach ($id[2] as $map) {
        $idcount++;

        $content = $this->getMap( $params, $map, $idcount, 0);

        $body = preg_replace('@{modmapyandex_id=' . $idcount . '}|{modmapyandex_calculator_id=' . $idcount . '}@si', $content, $body);
          
      }
    }

    return $body;
  }

  function nodeMap($params, $nid, $idcount, $type) {
    static $pluginParams = NULL;
    /*
    if ($type == ) {
    	drupal_add_js('http://api-maps.yandex.ru/2.0.10/?load=package.full&mode=debug&lang=ru-RU', array('type' => 'external', 'scope' => 'header', 'weight' => 1, 'cache' => TRUE));
    }*/
    $body = $this->getMap($params, $nid, $idcount, $type);

    return $body;
  }
}

